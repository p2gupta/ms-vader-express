package cl.guaman.vecommon.helper.exception;

import lombok.Getter;

@Getter
public class VaderExpressException extends RuntimeException {

    private final String code;

    public VaderExpressException(String code, String message) {
        super(message);
        this.code = code;
    }
}
