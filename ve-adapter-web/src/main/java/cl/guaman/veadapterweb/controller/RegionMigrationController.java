package cl.guaman.veadapterweb.controller;

import cl.guaman.veapplication.domain.usecase.migration.RegionMigrationUseCase;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(path = "/regions/migration", produces = MediaType.APPLICATION_JSON_VALUE)
public class RegionMigrationController {

    private final RegionMigrationUseCase regionMigrationUseCase;

    @PostMapping(path = "")
    public ResponseEntity fromApiToDb() {
        regionMigrationUseCase.fromApiToDb();
        return ResponseEntity.noContent().build();
    }
}
