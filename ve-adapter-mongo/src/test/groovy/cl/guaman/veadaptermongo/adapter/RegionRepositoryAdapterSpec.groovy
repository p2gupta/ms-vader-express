package cl.guaman.veadaptermongo.adapter

import cl.guaman.veadaptermongo.document.RegionDocument
import cl.guaman.veadaptermongo.repository.RegionRepository
import cl.guaman.veapplication.domain.Region
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import spock.lang.Specification

class RegionRepositoryAdapterSpec extends Specification {

    RegionRepositoryAdapter regionRepositoryAdapter
    RegionRepository regionRepository = Mock()

    def setup() {
        regionRepositoryAdapter = new RegionRepositoryAdapter(regionRepository)
    }

    def "save region's"() {
        when:
        Flux<Region> regions = Flux.just(Region.builder().id(1).name("test").build())
        regionRepository.findById(_) >> Mono.just(RegionDocument.builder().build())
        regionRepository.save(_) >> {}
        then:
        regionRepositoryAdapter.save(regions)
    }
}
