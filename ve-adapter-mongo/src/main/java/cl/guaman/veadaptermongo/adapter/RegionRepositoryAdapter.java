package cl.guaman.veadaptermongo.adapter;

import cl.guaman.veadaptermongo.document.RegionDocument;
import cl.guaman.veadaptermongo.repository.RegionRepository;
import cl.guaman.veapplication.domain.Region;
import cl.guaman.veapplication.domain.port.find.RegionFindPort;
import cl.guaman.veapplication.domain.port.update.RegionUpdatePort;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Objects;

@Slf4j
@Component
@AllArgsConstructor
public class RegionRepositoryAdapter implements RegionUpdatePort, RegionFindPort {

    private final RegionRepository regionRepository;

    @Override
    public void save(Flux<Region> regions) {
        regions.filter(item -> Objects.isNull(regionRepository.findById(item.getId()).block()))
                .map(item -> RegionDocument.builder()
                        .id(item.getId())
                        .name(item.getName())
                        .build())
                .doOnNext(item -> regionRepository.save(item).subscribe())
                .subscribe();
    }

    @Override
    public List<Region> findAll() {
        return regionRepository.findAll()
                .map(item -> Region.builder()
                        .id(item.getId())
                        .name(item.getName())
                        .build())
                .collectList()
                .block();
    }
}
