package cl.guaman.veconfig;

import cl.guaman.veadaptermongo.config.MongodbConfig;
import cl.guaman.veadapterclient.config.OpenFeignConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
@Import({OpenFeignConfig.class, MongodbConfig.class})
@ComponentScan(basePackages = {"cl.guaman"})
public class VeConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(VeConfigApplication.class, args);
    }

}
