[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pipeline status](https://gitlab.com/fguamanf/vader-express/badges/master/pipeline.svg)](https://gitlab.com/fguamanf/vader-express/-/commits/master)
[![coverage report](https://gitlab.com/fguamanf/vader-express/badges/master/coverage.svg)](https://gitlab.com/fguamanf/vader-express/-/commits/master)
[![Patreon: Fabián Guamán](https://img.shields.io/badge/patreon-fabi%C3%A1n%20guam%C3%A1n-orange)](https://www.patreon.com/fguaman)
[![API: chilexpress](https://img.shields.io/badge/api-chilexpress-yellow)](https://developers.wschilexpress.com/)

# vader-express
API reactiva para comunicación con chile express
## Diagrama de componentes
```plantuml
@startuml
skinparam component {
  BackgroundColor<<created>> #4CAF50
  BackgroundColor<<pending>> #03A9F4
  ArrowColor #546de5
  BorderColor #303030
}

cloud cloud
database mongodb

component apiChilexpress[
    api-chilexpress
]

package "ms-vader-express" {
    component veAdapterClient<<created>>[
        ve-adapter-client
    ]
    component veAdapterWeb<<created>>[
        ve-adapter-web
    ]
    component veConfig<<created>>[
        ve-config
    ]
    component veApplication<<created>>[
        ve-application
    ]
    component veCommon<<created>>[
        ve-common
    ]
    component veAdapterMongo<<created>>[
        ve-adapter-mongo
    ]
}

cloud -down-> veAdapterWeb : HTTP
veAdapterWeb --> veConfig
veAdapterClient -up-> veConfig
veApplication -left-> veConfig
veCommon -right-> veConfig
veAdapterMongo -up-> veConfig
veAdapterClient -down-> apiChilexpress : HTTP
veAdapterMongo -down-> mongodb
@enduml
```
