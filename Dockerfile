FROM openjdk:8-jre
EXPOSE 8080
ADD ve-config/build/libs/ve-config-0.0.1-SNAPSHOT.jar /app/app.jar
RUN apt-get update && apt-get install -y tzdata
WORKDIR /app
CMD java -jar app.jar
