package cl.guaman.veapplication.service.migration

import cl.guaman.veapplication.domain.port.find.RegionApiFindPort
import cl.guaman.veapplication.domain.port.update.RegionUpdatePort
import reactor.core.publisher.Flux
import spock.lang.Specification

class RegionMigrationServiceSpec extends Specification {

    RegionMigrationService regionMigrationService
    RegionApiFindPort regionApiFindPort = Mock()
    RegionUpdatePort regionUpdatePort = Mock()

    def setup() {
        regionMigrationService = new RegionMigrationService(
                regionApiFindPort,
                regionUpdatePort
        )
        regionApiFindPort.findAll() >> Flux.empty()
        regionUpdatePort.save(_) >> {}
    }

    def "region migration run"() {
        when:
        regionApiFindPort.findAll() >> Flux.empty()
        regionUpdatePort.save(_) >> {}
        then:
        regionMigrationService.run()
    }

    def "region migration from api to db"() {
        when:
        regionApiFindPort.findAll() >> Flux.empty()
        regionUpdatePort.save(_) >> {}
        then:
        regionMigrationService.fromApiToDb()
    }
}
