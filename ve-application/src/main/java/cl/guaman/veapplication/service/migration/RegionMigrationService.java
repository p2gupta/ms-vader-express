package cl.guaman.veapplication.service.migration;

import cl.guaman.veapplication.domain.Region;
import cl.guaman.veapplication.domain.port.find.RegionApiFindPort;
import cl.guaman.veapplication.domain.port.update.RegionUpdatePort;
import cl.guaman.veapplication.domain.usecase.migration.RegionMigrationUseCase;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

@Slf4j
@Service
@AllArgsConstructor
public class RegionMigrationService implements RegionMigrationUseCase, CommandLineRunner {

    private final RegionApiFindPort regionApiFindPort;
    private final RegionUpdatePort regionUpdatePort;

    @Async
    @Override
    public void run(String... args) throws Exception {
        fromApiToDb();
    }

    @Override
    public void fromApiToDb() {
        Flux<Region> regions = regionApiFindPort.findAll();
        regionUpdatePort.save(regions);
    }
}

