package cl.guaman.veapplication.domain.usecase.migration;

public interface RegionMigrationUseCase {

    void fromApiToDb();
}