package cl.guaman.veapplication.domain.port.find;

import cl.guaman.veapplication.domain.Region;
import reactor.core.publisher.Flux;

public interface RegionApiFindPort {

    Flux<Region> findAll();
}
