package cl.guaman.veapplication.domain.port.update;

import cl.guaman.veapplication.domain.Region;
import reactor.core.publisher.Flux;

public interface RegionUpdatePort {

    void save(Flux<Region> regions);
}
