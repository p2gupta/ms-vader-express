package cl.guaman.veapplication.domain.port.find;

import cl.guaman.veapplication.domain.Region;

import java.util.List;

public interface RegionFindPort {

    List<Region> findAll();
}
