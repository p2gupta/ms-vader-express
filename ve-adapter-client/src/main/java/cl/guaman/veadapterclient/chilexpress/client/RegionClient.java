package cl.guaman.veadapterclient.chilexpress.client;

import cl.guaman.veadapterclient.chilexpress.dto.RegionResponseDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "regionClient", url = "${chilexpress.url}")
public interface RegionClient {

    @GetMapping(path = "/regions", produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<RegionResponseDto> findAll();
}
