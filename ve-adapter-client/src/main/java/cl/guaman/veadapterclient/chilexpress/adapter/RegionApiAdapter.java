package cl.guaman.veadapterclient.chilexpress.adapter;

import cl.guaman.veadapterclient.chilexpress.client.RegionClient;
import cl.guaman.veadapterclient.chilexpress.dto.RegionResponseDto;
import cl.guaman.veapplication.domain.Region;
import cl.guaman.veapplication.domain.port.find.RegionApiFindPort;
import cl.guaman.vecommon.helper.exception.VaderExpressException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.Objects;

@Slf4j
@Component
@AllArgsConstructor
public class RegionApiAdapter implements RegionApiFindPort {

    private final RegionClient regionClient;

    @Override
    public Flux<Region> findAll() {
        ResponseEntity<RegionResponseDto> responseEntity = regionClient.findAll();
        if (!responseEntity.getStatusCode().is2xxSuccessful() || Objects.isNull(responseEntity.getBody())) {
            log.error("findAll | message={}", responseEntity.getBody());
            throw new VaderExpressException("RAA001", "error getting regions from api");
        }
        return Flux.fromIterable(responseEntity.getBody().getRegions())
                .map(item -> Region.builder()
                        .id(item.getIneRegionCode())
                        .name(item.getRegionName())
                        .build());
    }
}
