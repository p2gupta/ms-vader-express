package cl.guaman.veadapterclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VeAdapterClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(VeAdapterClientApplication.class, args);
    }

}
