package cl.guaman.veadapterclient.client;

import cl.guaman.veadapterclient.chilexpress.client.RegionClient;
import cl.guaman.veadapterclient.chilexpress.dto.RegionResponseDto;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

//@SpringBootTest
//@ActiveProfiles("test")
public class RegionClientTest {

    //@Autowired
    private RegionClient regionClient;

    //@Test
    public void findAllTest() {
        ResponseEntity<RegionResponseDto> responseEntity = regionClient.findAll();
        Assert.isTrue(responseEntity.getStatusCode().is2xxSuccessful(), "respuesta OK");
    }
}
